from funkload.FunkLoadTestCase import FunkLoadTestCase
import unittest


SERVER_HOST = '194.29.175.240' #194.29.175.240
SERVER_PORT = 6306 #6306
CRLF = '\r\n'

class SocketStub:

    def __init__(self, request):
        self.request = request
        self.response = ''
        self.offset = 0

    def recv(self, length):
        data = self.request[self.offset:self.offset+length]
        self.offset += length
        return data

    def send(self, response):
        self.response += response

    def sendall(self, response):
        self.response += response


class LoggerStub:

    def info(self, msg):
        pass

class Server(FunkLoadTestCase):

    def get_response(self):
        from full_http_server import handle_client
        socket_stub = SocketStub('GET / HTTP/1.1\r\nHost: example.com\r\n\r\n')
        logger_stub = LoggerStub()
        handle_client(socket_stub, logger_stub)
        return socket_stub.response

    def test_dialog(self):
        ok = self.get_response()
        expected = "200 OK"
        actual = ok.split(CRLF)[0].split(' ', 1)[1].strip()
        self.assertEqual(expected, actual)


if __name__ == '__main__':
    unittest.main()